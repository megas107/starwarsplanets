//
//  PlanetTableViewController.swift
//  StarWarsPlanets
//
//  Created by Miguel Ferreira on 10.10.18.
//  Copyright © 2018 Miguel Ferreira. All rights reserved.
//

import UIKit

class PlanetTableViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // Initialise planets from model
    var planets = [Planet]()
    var searchingPlanet = [Planet]()
    
    // When user search planet by name
    var searchActive : Bool = false
    
    // URL request to get all the planets ressources
    let url = URL(string: "https://swapi.co/api/planets/")!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get planets ressources and parse JSON response
        loadPlanets(url: url)

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // searchActive => true
        if searchActive {
            // Get searchingPlanet count
            return searchingPlanet.count
        } else {
            // Get planets count
            return planets.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // TableCell identifier
        let cellIdentifier = "PlanetTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlanetTableViewCell  else {
            fatalError("The dequeued cell is not an instance of PlanetTableViewCell.")
        }
        
        // searchActive => true
        if searchActive {
            // Get name and population from searchingPlanet
            cell.lblName.text = searchingPlanet[indexPath.row].name
            cell.lblPopulation.text = "Pop : " + searchingPlanet[indexPath.row].population
        } else {
            // Get name and population from planets
            cell.lblName.text = planets[indexPath.row].name
            cell.lblPopulation.text = "Pop : " + planets[indexPath.row].population
        }
        
        return cell
    }
    
    // MARK: - SearchBar
    // SearchBar click cancel button action
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Assign false value to searchActive
        searchActive = false
        // Reset searchBar text
        searchBar.text = ""
        // Hide keyboard
        self.searchBar.endEditing(true)
        // Reload tableView with new data
        tblView.reloadData()
    }
    
    // SearchBar click search button action
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        // Get text from searchBar
        let searchedPlanet = searchBar.text!

        // Iterate planets list
        for planet in planets {
            // If searchedPlanet exist on planets
            if searchedPlanet == planet.name {
                // Get planet
                searchingPlanet = [planet]
                // Assign true value to searchActive
                searchActive = true
                break
            }
        }
        // Reload tableView with new data
        tblView.reloadData()
        // Hide keyboard
        self.view.endEditing(true)
    }
    
    // MARK: - Planets functions
    // Get planets ressources and parse JSON response
    func loadPlanets(url: URL) {
        
        // Start spinner animation
        let sv = displaySpinner(onView: self.view)
        
        // Start network request call
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            guard let data = data, error == nil, response != nil else {
                print("Something is wrong")
                return
            }
            print("JSON downloaded")
            do {
                // Create JSONDecoder
                let decoder = JSONDecoder()
                // Decode JSON Response Data
                let model = try decoder.decode(Planets.self, from: data)
                // Append new planets
                self.planets += model.results
                // While "next" from json is not null, request next page
                if model.next != nil {
                    let sURL = URL(string: model.next!)
                    self.loadPlanets(url: sURL!)
                }
                // Sort planets alphabetically
                self.planets.sort(by: { $0.name < $1.name })
                
                // Reload tableView with new data
                DispatchQueue.main.async {
                    self.tblView.reloadData()
                }
                // Stop spinner animation
                self.removeSpinner(spinner: sv)
            } catch {
                print("Something wrong after downloaded")
            }
        }.resume()
    }
    
    // Update planets list
    @IBAction func refreshPlanets(_ sender: Any) {
        // Initialise planets
        planets = [Planet]()
        loadPlanets(url: url)
    }
    
    // MARK: - Loading spinner
    func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        //spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        // Create label
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
        label.text = "Loading planets..."
        label.center = spinnerView.center
        label.textAlignment = .center
        label.textColor = UIColor.yellow
        
        // Create ActivityIndicatorView
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        // Add subView
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            spinnerView.addSubview(label)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    func removeSpinner(spinner :UIView) {
        // Remove subView spinner
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? "") {
        
        case "showDetail":
            guard let planetDetailViewController = segue.destination as? PlanetDetailViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedPlanetCell = sender as? PlanetTableViewCell else {
                fatalError("Unexpected sender: \(String(describing: sender))")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedPlanetCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            var selectedPlanet: Planet
            
            // searchActive => true
            if searchActive {
                selectedPlanet = searchingPlanet[indexPath.row]
            } else {
                selectedPlanet = planets[indexPath.row]
            }
            planetDetailViewController.planet = selectedPlanet
            
        default:
            fatalError("Unexpected Segue Identifier; \(String(describing: segue.identifier))")
        }
    }
}
