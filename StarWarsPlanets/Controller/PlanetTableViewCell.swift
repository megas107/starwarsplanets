//
//  PlanetTableViewCell.swift
//  StarWarsPlanets
//
//  Created by Miguel Ferreira on 10.10.18.
//  Copyright © 2018 Miguel Ferreira. All rights reserved.
//

import UIKit

class PlanetTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPlanet: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPopulation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
