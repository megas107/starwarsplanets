//
//  ViewController.swift
//  StarWarsPlanets
//
//  Created by Miguel Ferreira on 05.10.18.
//  Copyright © 2018 Miguel Ferreira. All rights reserved.
//

import UIKit

class PlanetDetailViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var lblDetailPlanetName: UILabel!
    @IBOutlet weak var imgDetailPlanet: UIImageView!

    @IBOutlet weak var lblDetailPlanetPopulation: UILabel!
    @IBOutlet weak var lblDetailPlanetRotation: UILabel!
    @IBOutlet weak var lblDetailPlanetOrbital: UILabel!
    @IBOutlet weak var lblDetailPlanetDiameter: UILabel!
    @IBOutlet weak var lblDetailPlanetClimate: UILabel!
    @IBOutlet weak var lblDetailPlanetGravity: UILabel!
    @IBOutlet weak var lblDetailPlanetTerrain: UILabel!
    @IBOutlet weak var lblDetailPlanetSurface: UILabel!
    @IBOutlet weak var lblDetailPlanetCreated: UILabel!
    @IBOutlet weak var lblDetailPlanetEdited: UILabel!
    @IBOutlet weak var lblDetailPlanetURL: UILabel!

    // This value is either passed by `PlanetTableViewController` in `prepare(for:sender:)`
    var planet: Planet?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Set up views if editing an existing Planet
        if let planet = planet {
            lblDetailPlanetName.text = planet.name
            lblDetailPlanetPopulation.text = planet.population
            lblDetailPlanetRotation.text = planet.rotation_period
            lblDetailPlanetOrbital.text = planet.orbital_period
            lblDetailPlanetDiameter.text = planet.diameter
            lblDetailPlanetClimate.text = planet.climate
            lblDetailPlanetGravity.text = planet.gravity
            lblDetailPlanetTerrain.text = planet.terrain
            lblDetailPlanetSurface.text = planet.surface_water
            lblDetailPlanetCreated.text = planet.created
            lblDetailPlanetEdited.text = planet.edited
            lblDetailPlanetURL.text = planet.url
        }
    }
}
