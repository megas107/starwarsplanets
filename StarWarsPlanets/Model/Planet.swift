//
//  Planet.swift
//  StarWarsPlanets
//
//  Created by Miguel Ferreira on 10.10.18.
//  Copyright © 2018 Miguel Ferreira. All rights reserved.
//

import UIKit

class Planets: Codable {
    //MARK: Properties
    let count: Int
    let next: String?
    let previous: String?
    let results: [Planet]
    
    //MARK: Initialization
    init(count: Int, next: String, previous: String, results: [Planet]) {
        self.count = count
        self.next = next
        self.previous = previous
        self.results = results
    }
}

class Planet: Codable {
    
    //MARK: Properties
    let name: String
    let population: String
    let rotation_period: String
    let orbital_period: String
    let diameter: String
    let climate: String
    let gravity: String
    let terrain: String
    let surface_water: String
    let created: String
    let edited: String
    let url: String
    
    //MARK: Initialization
    init?(name: String, population: String, rotation_period: String, orbital_period: String, diameter: String, climate: String, gravity: String, terrain: String, surface_water: String, created: String, edited: String, url: String) {
        
        // Initialize stored properties.
        self.name = name
        self.population = population
        self.rotation_period = rotation_period
        self.orbital_period = orbital_period
        self.diameter = diameter
        self.climate = climate
        self.gravity = gravity
        self.terrain = terrain
        self.surface_water = surface_water
        self.created = created
        self.edited = edited
        self.url = url
    }
}
